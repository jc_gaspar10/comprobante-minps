package com.ach.bdsoportes.comprobante_minps.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ach.bdsoportes.comprobante_minps.utils.ApplicationProperties;


/**
 * Clase encargada de comprobar si existen los archivos MINPS que se reciben desde un csv y en caso de no existir escribe las rutas
 * en un archivo de texto
 * @author camilo.gaspar@alejandria-consulting.com
 *
 */
public class ComprobanteMain {

	private final static Logger logger = LogManager.getLogger(ComprobanteMain.class);

	/**
	 * Método principal encargado de comprobar si existen los archivos MINPS que se reciben desde un csv
	 *  y en caso de no existir escribe las rutas en un archivo de texto
	 * @param args ruta de archivo csv con rutas a los archivos MINPS
	 */
	public static void main(String[] args) {

		logger.info("Iniciando proceso de comprobación");
		long startTime = System.currentTimeMillis();

		if(args.length== 0){

			logger.error("No se adjuntó archivo csv como parámetro.");
		}
		else if(args.length > 0){

			if(args.length >1){
				logger.warn("Se agregó más de 1 archivo, se utilizará solo el primero");
			}
			if(!(args[0].endsWith(".csv"))){
				logger.warn("La ruta dada no corresponde a un archivo csv, sin embargo, se intentará utilizar.");
			}
			File file = new File(args[0]);

			if(file != null && file.exists()){

				String rutaArchivo = comprobar(file);

				if(rutaArchivo != null){

					logger.debug("Se realizó la comprobación y los archivos faltantes se encuentran en la siguiente ruta: "+rutaArchivo);
				}
				else{
					logger.error("No se creó el archivo con las rutas de los archivos faltantes");
				}
			} 
			else{
				logger.error("El archivo "+args[0]+" no existe.");
			}
		}

		long endTime = System.currentTimeMillis();
		long duration = (endTime - startTime)/1000;
		int hours = (int) duration / 3600;
		int remainder = (int) duration - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		logger.info("El proceso de comprobación ha finalizado y su duración fue de "
				+hours+(hours==1?" hora, ":" horas, ")+mins+(mins==1?" minuto":" minutos")+" y "+secs+(secs==1?" segundo.":" segundos."));
	}

	/**
	 * Método encargado de comprobar si existen los archivos MINPS que se reciben desde un csv
	 *  y en caso de no existir escribe las rutas en un archivo de texto
	 * @param archivo Ruta del archivo csv que contiene los archivos que se comprobaran
	 * @return Ruta del archivo en el que se escribieron las rutas de los archivos faltantes
	 */
	private static  String comprobar(File file) {


		FileReader fr = null;
		BufferedReader br = null;
		String nombreArchivoFaltantes = "faltantes_"+file.getName().substring(0,file.getName().lastIndexOf('.'))+".txt";
		String ubicacionArchivoFaltante = ApplicationProperties.getInstance().getValorPropiedad("ruta.destino");

		if(ubicacionArchivoFaltante != null){
			if(!(ubicacionArchivoFaltante.endsWith(File.separator))){
				ubicacionArchivoFaltante = ubicacionArchivoFaltante.concat(File.separator);
			}
			ubicacionArchivoFaltante = ubicacionArchivoFaltante.concat(nombreArchivoFaltantes);
			File fileFaltante = new File(ubicacionArchivoFaltante);
			try {
				if(fileFaltante.createNewFile()){

					int line = 1;

					try {
						fr = new FileReader(file);
						br = new BufferedReader(fr);

						String linea = br.readLine();

						while(linea != null){

							if(linea.equals("")){

								try{

									String rutaMinps = linea.split(";")[7];

									if(rutaMinps != null){

										File fileMinps = new File(rutaMinps);

										if(fileMinps!= null && !(fileMinps.exists())){
											escribirFaltantes(fileMinps.getAbsolutePath(), fileFaltante);
										}
									} else{
										logger.error("La ruta del archivo MINPS de la linea "+line+" es nula: "+rutaMinps);
									}
								} catch(IndexOutOfBoundsException e){
									logger.error("Formato de la linea "+line+" no es valido: "+linea,e);
								}
							} else{
								logger.error("La linea "+line+" esta vacia.");
							}

							line ++;
							linea = br.readLine();
						}


					} catch (FileNotFoundException e) {
						logger.error("Error al abrir el archivo para su lectura.",e);
					} catch (IOException e) {
						logger.error("Error al leer la linea "+line+" del archivo csv.",e);
					} finally {
						try{
							if(fr != null){
								fr.close();
							}
							if(br != null){ 
								br.close();
							}
						} catch (IOException e) {
							logger.error("Error al cerrar los recursos de lectura de archivos",e);
						}
					}
				}else{
					logger.error("Error el archivo "+fileFaltante.getAbsolutePath()+" ya existe");
					ubicacionArchivoFaltante = null;
				}
			} catch (IOException e) {

				logger.error("Error al crear el archivo con las rutas de los archivos faltantes",e);
			}
		}
		else{
			logger.error("Error obteniendo ruta destino desde el archivo de propiedades.");
		}

		return ubicacionArchivoFaltante;

	}

	/**
	 * Método encargado de escribir las rutas de los archivos faltantes en un archivo de texto
	 * @param rutaArchivo Ruta del archivo faltante
	 * @param fileFaltante Ruta del archivo en el que se escribirá el archivo faltante
	 */
	private static void escribirFaltantes(String rutaArchivo, File fileFaltante) {

		FileWriter fw = null;
		BufferedWriter bw = null;

		try{
			logger.debug("Iniciando escritura de archivo faltante.");
			fw = new FileWriter(fileFaltante,true);
			bw = new BufferedWriter(fw);

			bw.write(rutaArchivo);
			bw.newLine();
			bw.flush();

			logger.debug("Escritura de archivo faltante ha finalizado correctamente");
		} catch(IOException e){
			logger.error("Error al abrir el archivo "+fileFaltante.getAbsolutePath()+" para su escritura.",e);
		} finally {
			try{
				if(fw != null){
					fw.close();
				}
				if(bw != null){
					bw.close();
				}
			} catch (IOException e) {
				logger.error("Error al cerrar los recursos de escritura de archivos.",e);
			}
		}
	}
}

