# Comprobador MINPSs #

El proyecto Java cumple una función básica. Busca si los archivos dados por parámetro existen. En caso de no existir, escribe en un archivo de texto la ruta absoluta del archivo.

## Parámetros ##
El proyecto recibe un solo argumento que es la ruta del archivo csv que contiene las rutas a los archivos MINPS. 

Sin embargo, se le deben pasar dos parámetros a la JVM:

 - Ruta del archivo de Propiedades: La ruta al archivo de propiedades se debe pasar como parámetro a la JVM de la siguiente manera "-Dconfiguration.properties=C:\User\Desktop\file.properties". En el archivo de propiedades debe existir la propiedad "ruta.destino" y el valor debe ser la ruta en la que se desea almacenar el archivo con las rutas de los archivos que no existen.
 
 - Ruta del archivo de configuración log4j: La ruta al archivo que se encarga de la configuración de Log4j se debe pasar como parámetro a la JVM de la siguiente manera "-Dlog4j.configurationFile=C:\User\Desktop\log4jconfig.xml ". Consultar sobre este archivo en: https://logging.apache.org/log4j/2.x/manual/configuration.html

## Ejecución ##
La ejecución se realiza ejecutando el siguiente comando:

```bash
 java -Dconfiguration.properties="/ruta/al/properties" -Dlog4j.configurationFile="/ruta/al/archivo/config" -jar bdsoportes-comprobante-minps.jar [archivo.csv]
```
## Resultado ##
Al ejecutar el proyecto correctamente debería crearse un archivo .log con el nombre que se le dio en el archivo de configuración de log4j. Si el log se deja en un nivel de debug se debería apreciar todo el proceso.
Si al ejecutar el proyecto no existen algunos archivos las rutas de estos quedaran almacenadas en la ubicación que se estableció en el archivo de propiedades y su nombre sera el mismo del archivo que se pasa como argumento precedido de "faltantes_". Si existen todos lo archivos se creará el archivo resultado de todas formas solo que estará vacío. 